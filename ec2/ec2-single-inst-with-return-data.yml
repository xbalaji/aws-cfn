---

# 
# Filename    : ec2-single-inst-with-return-data.yml
# Date        : 07 Oct 2018
# Author      : Balaji Venkataraman (xbalaji@gmail.com)
# Description : 
#   Create a simple ec2 instance, return data from instance to stack output
#   Start the python simple webserver on a screen session 
#
# Usage:
#   AWS console->CloudFormation->Create stack->upload this file
#   Output tab will display the IP address of the instance
#

AWSTemplateFormatVersion: "2010-09-09"
Description: "Simple Single EC2 instance on the selected VPC, Subnet"

Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - Label: {default: "General tags"}
        Parameters: [pMyIp, pEnvironment, pBusinessUnit]
      - Label: {default: "Instance configuration"}
        Parameters: [pInstanceType, pOsType, pKeyName]
      - Label: {default: "Network related configuration"}
        Parameters: [pVpcId, pSubNetPub1]
    ParameterLabels:
      pMyIp: {default: "IP address "}
      pEnvironment: {default: "Environment" }
      pBusinessUnit: {default: "Business organization" }
      pInstanceType: {default: "Instance Type" }
      pOsType: {default: "Operating system" }
      pKeyName: {default: "Key pair" }
      pVpcId: {default: "VPC to use" }
      pSubNetPub1: {default: "Public  Subnet to use in zone A" }
Parameters:
  pMyIp:
    Description: "Current system's IP check using, http://checkip.amazonaws.com/ "
    Type: String
    MinLength: 9
    MaxLength: 18
    Default: "0.0.0.0/0"
    AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    ConstraintDescription: "Must be a valid IP CIDR range of the form x.x.x.x/x."
  pEnvironment:
    Type: String
    Default: Dev
    AllowedValues:
    - Dev
    - Test
    - Prod
    - Tools
  pBusinessUnit:
    Type: String
    Default: BU1
    AllowedValues:
    - BU1
    - BU2
  pInstanceType: 
    Description: "EC2 instance type"
    Default: t2.small
    Type: String
    AllowedValues: 
      - t2.micro
      - t2.small
      - t2.medium
      - "--- CJOC - CJE ---"
      - m5.large
      - m5.xlarge
      - r3.xlarge
      - "--- Not frequenty used ---"
      - t1.micro
      - t2.nano
      - t2.large
  pOsType:
    Description: "Amazon Linux only (cf-init)"
    Type: String
    Default: AwsLinux
    AllowedValues: 
      - AwsLinux
  pKeyName:
    Description: "The EC2 Key Pair to allow SSH access to the instance"
    Type: "AWS::EC2::KeyPair::KeyName"
    Default: "xb_aws_pge_gen01"
  pVpcId:
    Description: "VPC for this instance"
    Type: "AWS::EC2::VPC::Id"
  pSubNetPub1:
    Description: "Public Zone-A subnet for this instance"
    Type: "AWS::EC2::Subnet::Id"

Mappings:
  OsToAMIID:
    AwsLinux:
      # https://aws.amazon.com/marketplace/pp/B00CIYTQTC
      "id": "ami-0ad99772"

Resources:
  rSshHttpHttpsSG:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: "Enable SSH, HTTP, HTTPS - xbalaji"
      VpcId: !Ref pVpcId
      SecurityGroupIngress:
      - IpProtocol: "tcp"
        FromPort: "22"
        ToPort: "22"
        CidrIp: !Ref pMyIp
      - IpProtocol: "tcp"
        FromPort: "80"
        ToPort: "80"
        CidrIp: !Ref pMyIp
      - IpProtocol: "tcp"
        FromPort: "443"
        ToPort: "443"
        CidrIp: !Ref pMyIp
      Tags:
        - Key: Name
          Value:
            Fn::Join:
            - "-"
            - - Ref: pBusinessUnit
              - Ref: pEnvironment
              - "SG"
        - Key: BusinessUnit
          Value: !Ref pBusinessUnit
        - Key: Environment
          Value: !Ref pEnvironment

  rDataWaitHandle:
    Type: "AWS::CloudFormation::WaitConditionHandle"
    Metadata:
      "Comment1": "Reference: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-waitcondition.html"

  rDataWaitCondition:
    Type: "AWS::CloudFormation::WaitCondition"
    DependsOn: rEc2InstancePub1
    Properties:
      Handle: !Ref rDataWaitHandle
      Timeout: "300"  # 300 seconds = 5 minutes

  rEc2InstancePub1:
    Type: "AWS::EC2::Instance"
    Metadata:
      "Comment1": "Reference: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-init.html"
      AWS::CloudFormation::Init:
        configSets:
          default: [install, configure, run]
        install:
          packages:
            yum:
              wget: []
          files:
            "/tmp/webserver/private-dns-name.txt":
              content: |
                #
                # Attempt to find private dns name, similar to what is seen in AWS console 
                #                                                        xbalaji@gmail.com 
                # 
                # From:
                # curl http://169.254.169.254/latest/meta-data/hostname 
                # 
                # Typically the following should give it:
                # curl http://169.254.169.254/latest/meta-data/local-hostname
                # 

              mode: "000644"
              owner: "root"
              group: "root"
        configure:
          commands:
            a_dns_a_getregion:
              command: 'echo ${ENV_AWS_REGION} > /tmp/webserver/env-aws-region.txt'
              env:
                ENV_AWS_REGION: !Sub '${AWS::Region}'
            a_dns_c_getlocalip:
              command: 'curl http://169.254.169.254/latest/meta-data/hostname | cut -d "." -f1-1 > /tmp/webserver/env-hostname.txt'
        run:
          commands:
            b_make_a_dnsname:
              command: "echo `cat /tmp/webserver/env-hostname.txt`.`cat /tmp/webserver/env-aws-region.txt`.compute.internal > /tmp/webserver/dns-name.txt"
            b_make_c_dnsname:
              command: "echo `cat /tmp/webserver/dns-name.txt` >> /tmp/webserver/private-dns-name.txt"
    Properties:
      #Condition: cEc2PubZoneA
      # privatedns=\$\(cat /tmp/webserver/private-dns-name.txt\)
      ImageId: !FindInMap [OsToAMIID, !Ref pOsType, "id" ]
      InstanceType: !Ref pInstanceType
      KeyName: !Ref pKeyName
      SubnetId: !Join [ "", [ "", !Ref pSubNetPub1 ] ]
      SecurityGroupIds: [ !GetAtt [ rSshHttpHttpsSG, GroupId ] ]
      UserData:
        "Fn::Base64": !Sub |
          #!/bin/bash -x
          yum update -y
          mkdir -p /tmp/webserver
          ifconfig -a > /tmp/webserver/ifconfig.txt
          cd /tmp/webserver; screen -dmS pyserv python -m SimpleHTTPServer 80
          /opt/aws/bin/cfn-init   -v    --stack ${AWS::StackName} --resource rEc2InstancePub1 --region ${AWS::Region}
          privatedns=$(cat /tmp/webserver/dns-name.txt)
          /opt/aws/bin/cfn-signal -e $? -d $privatedns  -r "Private DNS Complete" "${rDataWaitHandle}"
      Tags:
        - Key: Name
          Value:
            Fn::Join:
            - "-"
            - - Ref: pBusinessUnit
              - Ref: pEnvironment
              - "EC2"
        - Key: BusinessUnit
          Value: !Ref pBusinessUnit
        - Key: Environment
          Value: !Ref pEnvironment
Outputs:
  oEc2Pub1Az:
    Value: !Join [ "", [ "", !GetAtt rEc2InstancePub1.AvailabilityZone ] ]
  oEc2Pub1PrvIp4: 
    Value: !Join [ "", [ "", !GetAtt rEc2InstancePub1.PrivateIp ] ]
  oEc2Pub1PrvName:
    Value: !Join [ "", [ "", !GetAtt rEc2InstancePub1.PrivateDnsName ] ]
  oEc2Pub1PubIpv4: 
    Value: !Join [ "", [ "", !GetAtt rEc2InstancePub1.PublicIp ] ]
  oEc2Pub1PubName:
    Value: !Join [ "", [ "", !GetAtt rEc2InstancePub1.PublicDnsName ] ]
  oWaitDataJson:
    Description: "Data from the EC2 instance"
    Value: !GetAtt [ "rDataWaitCondition", "Data" ]

